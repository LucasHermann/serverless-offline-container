# Simple container to run Serveless locally
FROM node:8-alpine

RUN apk update && \
	npm install -g serverless \
	npm install -g serverless-offline \
	npm install -g yarn
